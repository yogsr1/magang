<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function dashboardPage()
    {
        return view('dashboardPage',[
            'user' => auth()->user()
        ]);
    }
}
