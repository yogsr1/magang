<?php

namespace App\Http\Controllers;

use App\Hobby;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SignUpController extends Controller
{
    public function signUpPage()
    {
        return view('signUpPage');
    }
    
    public function signUp(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'confirmed',
            'email' => 'required',
            'umur' => 'required'
        ]);

        $requestData = $request->all();
        
        $user = User::create([
            'username' => $requestData['username'],
            'namaLengkap' => $requestData['namaLengkap'],
            'email' => $requestData['email'],
            'password' => Hash::make($requestData['password']),
            'umur' => $requestData['umur'],
            'gender' => $requestData['gender'],
            'file' => $requestData['file']
        ]);

        foreach ($requestData['hobby'] as $hobby){
            Hobby::create([
                'user_id' => $user->id,
                'hobby_name' => $hobby
            ]);
        }
           
        return redirect()->route('loginPage');
    }
}
