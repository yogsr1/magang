<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/login','LoginController@loginPage')->name('loginPage');
Route::post('/login','LoginController@login')->name('loginSubmit');

Route::get('/signUp','SignUpController@signUpPage')->name('signUpPage');
Route::post('/signUp','SignUpController@signUp')->name('signUp');

Route::middleware('auth')->group(function() {
    Route::get('dashboard','DashboardController@dashboardPage')->name('dashboardPage');

    Route::get('logout','LoginController@logout')->name('logout');
});