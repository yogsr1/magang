<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Bank Bang Tut
                </div>
                <ul>
                    @if (!empty($errors->all()))
                    @foreach ($errors->all() as $error)
                    <li> {{ $error }}</li>
                    @endforeach
                    @endif
                </ul>
                <form method="POST" action="{{ route('signUp') }}">
                   @csrf
                   <div style="margin-bottom: 20px;">
                    <label for="username">Username</label>
                    <input required type="text" name="username">
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="password">Password</label>
                       <input required type="password" name="password">
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="confrimPassword">ConfrimPassword</label>
                       <input required type="Password" name="password_confirmation">
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="namaLengkap">Nama Lengkap</label>
                       <input type="text" name="namaLengkap">
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="email">Email</label>
                       <input type="email" name="email">
                   </div>
                   <div style="margin-bottom: 20px;">
                        <label for="umur">Umur</label>
                       <input type="number" name="umur">
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="gender">Jenis Kelamin</label>
                       <input type="radio" name="gender" value="Laki-Laki">Laki-Laki
                       <input type="radio" name="gender" value="Perempuan">Perempuan
                   </div>
                   <div style="margin-bottom: 20px;">
                       <label for="hobby">Hobi</label>
                       <input type="checkbox" name="hobby[]" value="Berenang">Berenang
                       <input type="checkbox" name="hobby[]" value="Basket">Basket
                       <input type="checkbox" name="hobby[]" value="Futsal">Futsal
                   </div>
                   <div style="margin-bottom: 20px;">
                   <label for="image">Foto KTP</label>
                   <input type="file" name="file">
                </div>
                   <div>
                       <input type="submit" name="signUp" value="Sign Up">
                   </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>